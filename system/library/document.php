<?php

/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
 */

/**
 * Document class
 */
class Document
{
	private $title;
	private $description;
	private $keywords;
	private $links = array();
	private $styles = array();
	private $scripts = array();

	public function __construct($registry)
	{
		$this->compress = $registry->get('compress');
	}

	/**
	 * 
	 *
	 * @param	string	$title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * 
	 * 
	 * @return	string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * 
	 *
	 * @param	string	$description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * 
	 *
	 * @param	string	$description
	 * 
	 * @return	string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * 
	 *
	 * @param	string	$keywords
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}

	/**
	 *
	 * 
	 * @return	string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}

	/**
	 * 
	 *
	 * @param	string	$href
	 * @param	string	$rel
	 */
	public function addLink($href, $rel='preload')
	{
		$this->links[$href] = array(
			'href' => $href,
			'rel'  => $rel
		);
	}

	/**
	 * 
	 * 
	 * @return	array
	 */
	public function getLinks()
	{
		return $this->links;
	}

	/**
	 * 
	 *
	 * @param	string	$href
	 * @param	string	$rel
	 * @param	string	$media
	 */
	public function addStyle($href, $rel = 'stylesheet', $media = 'screen')
	{
		$this->styles[$href] = array(
			'href'  => $href,
			'rel'   => $rel,
			'media' => $media
		);
	}

	/**
	 * 
	 * 
	 * @return	array
	 */
	public function getStyles()
	{
		//return $this->styles;

		$md5 = md5(serialize($this->styles)) . '.css';
		$file = DIR_CACHE . $md5 . '.css';

		if (file_exists($file)) {
			$style[$file] = array(
				'href' => $file,
				'rel' => 'stylesheet',
				'media' => 'screen'
			);

			return $style;
		} else {

			$resource = 'system/storage/cache/' . $this->compress->merge_css($this->styles);

			$array[$resource] = array(
				'href' => $resource,
				'rel' => 'stylesheet',
				'media' => 'screen'
			);

			return $array;
		}
	}

	/**
	 * 
	 *
	 * @param	string	$href
	 * @param	string	$postion
	 */
	public function addScript($href, $postion = 'header', $sort = 1)
	{
		$this->scripts[$postion][$href] = $href;
	}

	/**
	 * 
	 *
	 * @param	string	$postion
	 * 
	 * @return	array
	 */
	public function getScripts($postion = 'header')
	{
		if (isset($this->scripts[$postion])) {
			//return $this->scripts[$postion];
			// echo '<pre>';
			// var_dump($this->scripts);
			// echo '</pre>';
			$md5 = md5(serialize($this->scripts[$postion])) . '.js';
			$file = DIR_CACHE . $md5 . '.js';

			if (file_exists($file)) {
				$scripts[$file] = $file;
				return $scripts;
			} else {
				$resource = 'system/storage/cache/' . $this->compress->merge_js($this->scripts[$postion]);
				$array[$resource] = $resource;
				return $array;
			}
		} else {
			return array();
		}
	}
}
